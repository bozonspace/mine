### To start frontend [React JS, Typescript, Tailwind]
`yarn install`
`yarn watch`

### To start back [Laravel]
`composer install`
make .env file
`php artisan key:generate`
`php artisan serve`

### Look at <http://localhost:8000/>

### Login as admin
`admin@email.com
password`

### Example of Laravel response
`return [
            'data' => $posts,
            'success' => false,
            'message' => ''
        ];`

### After deploying on prod server
`php artisan key:generate.
php artisan config:clear,
php artisan config:cache
php artisan route:clear`