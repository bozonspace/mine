<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\VerifyEmailController;
use App\Models\Post;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route for email confirmation
Route::get('verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
    ->middleware(['signed', 'throttle:6,1'])
    ->name('verification.verify');

// Protected Routes for Authorized Users
Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {
    Route::get('/post/create', function () {
        return view('index');
    })->name('post-create');
    Route::get('/post/edit/{id}', [PostController::class, 'postEdit'])->name('post-edit');
});

// Public Routes
Route::get('/login', function () {return view('index');})->name('login');
Route::get('/post/{id}', function (Request $request) {
    $post = Post::find($request->route('id'));
    if($post) return view('index');
    return redirect('/');
});

// Plug
Route::get( '/{any}', function () {
    return view('index');
})->where('any', '.*');

// Route::get('/email/verify', function () {
//     // return view('auth.verify-email');
//     return view('index');
// })->middleware('auth')->name('verification.notice');
