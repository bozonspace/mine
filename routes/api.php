<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Public Routes
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::get('/get-posts', [PostController::class, 'getPosts']);
Route::get('/get-post', [PostController::class, 'getPost']);

// Protected Routes for Authorized Users
Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/get-me', [UserController::class, 'getMe']);

    Route::post('/post-create', [PostController::class, 'postCreate']);
    Route::post('/post-update', [PostController::class, 'postUpdateAPI']);
    Route::post('/post-delete', [PostController::class, 'postDelete']);
});


// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });