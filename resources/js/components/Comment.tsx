import React from 'react';
import { CommentI } from '../helpers/interfaces';

function Comment({ cmt }: {cmt:CommentI}) {
  const {
    id, comment, postId, userId, user, updatedAt,
  } = cmt;
  return (
    <div className="border-1 border-darkGray-10 rounded-lg p-2 odd:bg-darkGray-5">
      <div className="flex gap-2">
        <div className="gap-0.1 min-w-20 max-w-17">
          <div className="m-auto w-10 h-10">
            <img className="rounded-full w-full h-full" src="/assets/placeholder.png" alt="" />
          </div>
          <span className="block text-center">{user?.nickname}</span>
        </div>
        <div className="">
          {comment}
        </div>
      </div>
      <div className="mt-1 w-full flex items-center justify-end gap-3 text-cardTag text-darkGray-60 ">
        <div title="Reply" className="flex items-center space-x-0.5">
          <svg className="h-4" data-info="Icon by Gayrat Muminov on freeicons.io" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
            <path d="M8.334 2.5h3.333a6.667 6.667 0 1 1 0 13.333v2.917c-4.167-1.667-10-4.167-10-9.583A6.667 6.667 0 0 1 8.334 2.5ZM10 14.167h1.667a5.001 5.001 0 0 0 0-10H8.334a5 5 0 0 0-5 5c0 3.008 2.051 4.971 6.666 7.066v-2.066Z" fill="#788EA5" />
          </svg>
          <span className="">
            Reply
          </span>
        </div>
        <div className="flex items-center space-x-0.5">
          <span title="Like">
            <svg className="h-4" data-info="Icon by Gayrat Muminov on freeicons.io" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22">
              <g clipPath="url(#a)"><path fillRule="evenodd" clipRule="evenodd" d="M13.252 3.704a.971.971 0 0 0-1.158.546L10.467 7.97A5.877 5.877 0 0 1 9.22 9.746a5.288 5.288 0 0 1-1.01.786c.025.153.039.309.039.468v6.16c1.802.458 3.176.712 4.469.783 1.448.08 2.835-.072 4.642-.478a1.772 1.772 0 0 0 1.33-1.393l1.055-5.273a.917.917 0 0 0-.898-1.096H15.29a1.833 1.833 0 0 1-1.809-2.135l.461-2.77a.972.972 0 0 0-.69-1.094Zm-5.46 15.232c1.857.472 3.364.758 4.825.838 1.666.091 3.227-.089 5.145-.52 1.403-.317 2.45-1.444 2.726-2.824l1.055-5.27a2.75 2.75 0 0 0-2.697-3.291h-3.557l.462-2.769a2.805 2.805 0 0 0-5.337-1.585L8.786 7.236a4.052 4.052 0 0 1-1.474 1.696A2.74 2.74 0 0 0 5.5 8.25H2.75A2.75 2.75 0 0 0 0 11v6.417a2.75 2.75 0 0 0 2.75 2.75H5.5a2.747 2.747 0 0 0 2.292-1.231ZM2.75 10.083a.917.917 0 0 0-.917.917v6.417a.917.917 0 0 0 .917.916H5.5a.917.917 0 0 0 .917-.916V11a.915.915 0 0 0-.917-.917H2.75Z" fill="#788EA5" /></g>
              <defs><clipPath id="a"><path fill="#fff" d="M0 0h22v22H0z" /></clipPath></defs>
            </svg>
          </span>
          <span title="Dislike">
            <svg className="h-4" data-info="Icon by Gayrat Muminov on freeicons.io" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22">
              <g clipPath="url(#a)"><path fillRule="evenodd" clipRule="evenodd" d="M8.748 18.296a.971.971 0 0 0 1.158-.546l1.627-3.721a5.874 5.874 0 0 1 1.246-1.775 5.284 5.284 0 0 1 1.01-.786A2.832 2.832 0 0 1 13.75 11V4.84c-1.802-.458-3.176-.712-4.469-.783-1.448-.08-2.835.072-4.642.478-.666.15-1.19.694-1.33 1.393l-1.055 5.273a.917.917 0 0 0 .898 1.096H6.71a1.832 1.832 0 0 1 1.809 2.135l-.461 2.77a.972.972 0 0 0 .69 1.094Zm5.46-15.232c-1.857-.472-3.364-.758-4.825-.838-1.666-.091-3.227.089-5.145.52-1.403.317-2.45 1.444-2.726 2.824L.457 10.84a2.75 2.75 0 0 0 2.697 3.291h3.557L6.249 16.9a2.805 2.805 0 0 0 5.337 1.585l1.628-3.721a4.053 4.053 0 0 1 1.474-1.696c.5.44 1.145.683 1.812.682h2.75A2.75 2.75 0 0 0 22 11V4.583a2.75 2.75 0 0 0-2.75-2.75H16.5a2.746 2.746 0 0 0-2.292 1.231Zm5.042 8.853a.917.917 0 0 0 .917-.917V4.583a.917.917 0 0 0-.917-.916H16.5a.917.917 0 0 0-.917.916V11a.914.914 0 0 0 .917.917h2.75Z" fill="#788EA5" /></g>
              <defs><clipPath id="a"><path fill="#fff" transform="rotate(-180 11 11)" d="M0 0h22v22H0z" /></clipPath></defs>
            </svg>
          </span>
        </div>
        <div className="">
          {updatedAt}
        </div>
      </div>
    </div>
  );
}

export default Comment;
