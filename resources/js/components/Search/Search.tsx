import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch } from '../../redux/hooks';
import { getPostsSearchAsync } from '../../redux/slicers/postsSlice';

function Search({ text }:{text?:string}) {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const [search, setSearch] = useState<string>(text || '');

  const onChangeInput = (e:React.BaseSyntheticEvent) => {
    const v = e.currentTarget.value;
    setSearch(v);
  };

  const clear = () => { setSearch(''); };
  const thunkProps = {
    offset: 0,
    search,
  };

  const onSubmit = (e: React.ChangeEvent<any>) => {
    e.preventDefault();
    // dispatch(getPostsSearchAsync(thunkProps));
    navigate(`/?search=${encodeURI(search)}`, { replace: true });
  };
  return (
    <form onSubmit={onSubmit} className=" flex items-center">
      <div className="flex items-center space-x-2 border-darkGray-10 border-1 rounded-l-lg p-2">
        <div className="w-7 h-7">
          <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 29">
            <path d="M11.6 21.739a9.955 9.955 0 0 0 6.474-2.36l7.869 7.867a.9.9 0 0 0 .651.27.923.923 0 0 0 .652-1.574l-7.868-7.868a10.122 10.122 0 0 0 2.36-6.474c0-5.598-4.54-10.139-10.138-10.139C6.025 1.461 1.46 6.025 1.46 11.6c0 5.598 4.564 10.139 10.139 10.139Zm0-18.434c4.586 0 8.295 3.731 8.295 8.295a8.29 8.29 0 0 1-8.295 8.295c-4.586 0-8.295-3.731-8.295-8.295S7.036 3.305 11.6 3.305Z" fill="#B6C1CE" />
          </svg>
        </div>
        <div>
          <input
            className="w-128 focus:border-0"
            value={search}
            onChange={onChangeInput}
            placeholder="Search by #tag, title, @author..."
          />
        </div>
        <button
          type="button"
          className={`w-5 h-5 cursor-pointer ${!search?.length ? 'opacity-0' : ''}`}
          onClick={clear}
        >
          <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18">
            <path d="M9 2.953A6.055 6.055 0 0 0 2.953 9c.332 8.021 11.762 8.018 12.094 0A6.056 6.056 0 0 0 9 2.953Zm2.897 8.348a.422.422 0 0 1-.596.596L9 9.597l-2.3 2.3a.422.422 0 0 1-.597-.596L8.403 9l-2.3-2.3a.422.422 0 1 1 .596-.597L9 8.403l2.3-2.3a.422.422 0 0 1 .597.596L9.597 9l2.3 2.3Z" fill="#394756" />
          </svg>
        </button>
      </div>
      <button type="submit" className="bg-blue-30 hover:bg-blue-40 text-white font-semibold px-2 py-2.75 rounded-r-lg">
        Go
      </button>
    </form>
  );
}
Search.defaultProps = {
  text: '',
};
export default Search;
