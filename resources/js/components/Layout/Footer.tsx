import React from 'react';

function Footer() {
  return (
    <div id="footer" className="flex justify-between space-x-2 w-full bg-white">
      <div className="text-black text-center py-3 border-t-1 border-darkGray-30 w-full">
        {`TalkIn, All rights reserved, ${new Date().getFullYear()}`}
      </div>
    </div>
  );
}

export default Footer;
