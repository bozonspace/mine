import React from 'react';

import { useAppSelector } from '../../../../redux/hooks';
import { selectUser } from '../../../../redux/slicers/userSlice';

function UserMenu() {
  const user = useAppSelector(selectUser);
  return (
    <div>
      <div className="w-11 h-11 bg-darkGray-10 rounded-full flex items-center justify-center text-h2">
        {user?.nickname?.charAt(0)?.toUpperCase()}
      </div>
    </div>
  );
}

export default UserMenu;
