import axios from 'axios';
import React from 'react';
import { Link } from 'react-router-dom';

import { useAppSelector } from '../../../redux/hooks';
import { selectUser } from '../../../redux/slicers/userSlice';

function LoginReg() {
  const user = useAppSelector(selectUser);
  const logout = () => {
    axios.post('/api/logout', {}, {}).then((res) => {
      if (res.data.success) {
        window.location = res.data.path;
      }
    });
  };
  return (
    <div className="flex gap-1">
      {
        user?.id === 0 ? (
          <>
            <Link to="/login">
              <button type="button" className="text-cardTag p-1 border-1 rounded border-darkGray-10 hover:bg-darkGray-10">
                Login
              </button>
            </Link>
            <Link to="/register">
              <button type="button" className="text-cardTag p-1 border-1 rounded border-darkGray-10 hover:bg-darkGray-10">
                Register
              </button>
            </Link>
          </>
        ) : (
          <button type="button" onClick={logout} className="text-cardTag p-1 border-1 rounded border-darkGray-10 hover:bg-darkGray-10">
            Logout
          </button>
        )
      }

    </div>
  );
}

export default LoginReg;
