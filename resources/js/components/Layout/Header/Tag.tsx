import React from 'react';

function Tag({ tag }:{tag:string}) {
  return (
    <div className="text-headerTag hover:bg-darkGray-10 p-2 rounded-lg">
      {`#${tag}`}
    </div>
  );
}

export default Tag;
