import React from 'react';
import { Link } from 'react-router-dom';
import { makeid } from '../../../helpers/helpers';
import { useAppSelector } from '../../../redux/hooks';
import { selectUser } from '../../../redux/slicers/userSlice';

import Bell from './Bell/Bell';
import LoginReg from './LoginReg';
import Tag from './Tag';
import UserMenu from './UserMenu/UserMenu';

function Header() {
  const user = useAppSelector(selectUser);
  const tags: Array<string> = ['sport', 'dance', 'work', 'money', 'soul', 'music', 'game'];
  return (
    <div id="header" className="flex items-center justify-between flex-wrap gap-5 w-full py-3 px-3 shadow-md bg-white sticky top-0">
      <div className="font-black text-blue-30 text-logo">
        <Link to="/">
          Talkin
        </Link>
      </div>
      <div className="flex flex-wrap gap-2">
        {
           Array.isArray(tags) ? tags.map((e:any) => <Tag key={makeid(10)} tag={e} />) : ''
        }
      </div>
      <div className="flex items-center space-x-3">
        <LoginReg />
        <Bell />
        <Link to={`${user?.id === 0 ? '/login' : '/post/create'}`}>
          <div className="w-6 h-6" title="Create a discussion">
            <svg data-info="Icon by Fasil on freeicons.io" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25">
              <g clipPath="url(#a)">
                <path d="M21 11h1v12.75A1.26 1.26 0 0 1 20.75 25H1.25A1.25 1.25 0 0 1 0 23.75V4.25A1.25 1.25 0 0 1 1.25 3H14v1H1.25a.25.25 0 0 0-.25.25v19.5a.25.25 0 0 0 .25.25h19.5a.25.25 0 0 0 .25-.25V11ZM8.72 12.87 8 16.4a.5.5 0 0 0 .49.6h.1l3.5-.71a.5.5 0 0 0 .25-.14L22.68 5.82l-3.5-3.5-10.33 10.3a.5.5 0 0 0-.13.25ZM24.54 1.75 23.25.46a1.56 1.56 0 0 0-2.2 0l-1.17 1.15 3.5 3.5L24.54 4a1.56 1.56 0 0 0 0-2.2v-.05Z" fill="#788EA5" />
              </g>
              <defs><clipPath id="a"><path fill="#fff" d="M0 0h25v25H0z" /></clipPath></defs>
            </svg>
          </div>
        </Link>
        <UserMenu />
      </div>

    </div>
  );
}

export default Header;
