import React from 'react';
import { Link } from 'react-router-dom';
import { makeid, shortenIt } from '../../helpers/helpers';
import { CardI, TagI } from '../../helpers/interfaces';

interface IncomeProps {
    card: CardI
}

function Card({ card }:IncomeProps) {
  const {
    title, tags, bookmark, views, comments, updatedAt, id,
  } = card;
  return (
    <div className="shadow hover:shadow-md rounded-lg p-5 h-min">
      <Link to={`/post/${id}`}>
        <div className="text-headerTag font-semibold">
          {shortenIt(title, 40)}
        </div>
      </Link>
      <div className="mt-2 flex flex-wrap">
        {
            Array.isArray(tags) ? tags.map((e:TagI) => (
              <Link to={`/?search=#${encodeURI(e.tag)}`} key={makeid(10)}>
                <div className="hover:bg-darkGray-10 px-1 py-0.25 rounded-lg">
                  {`#${e.tag}`}
                </div>
              </Link>
            )) : ''
        }
      </div>
      <Link to={`/post/${id}`}>
        <div className="text-blue-30 hover:underline">
          more
        </div>
      </Link>
      <div className="mt-0.25 flex items-center flex-wrap justify-between space-x-2.5 text-darkGray-40">
        <div className="text-cardTag  text-left">
          {updatedAt}
        </div>
        <div className="flex items-center space-x-2.5">
          <div className="flex items-center space-x-0.5">
            <svg className="h-4" data-info="Icon by Raj Dev on freeicons.io" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 21">
              <path d="M16.625 18.375 10.5 14l-6.125 4.375v-14a1.75 1.75 0 0 1 1.75-1.75h8.75a1.75 1.75 0 0 1 1.75 1.75v14Z" stroke="#788EA5" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
            <span className="text-cardTag ">
              {bookmark}
            </span>
          </div>
          <div className="flex items-center space-x-0.5">
            <svg className="h-4" data-info="Icon by Raj Dev on freeicons.io" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
              <g clipPath="url(#a)" stroke="#788EA5" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                <path d="M.833 10S4.167 3.333 10 3.333 19.167 10 19.167 10 15.833 16.667 10 16.667.833 10 .833 10Z" />
                <path d="M10 12.5a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z" />
              </g>
              <defs><clipPath id="a"><path fill="#fff" d="M0 0h20v20H0z" /></clipPath></defs>
            </svg>
            <span className="text-cardTag">
              {views}
            </span>
          </div>
          <div className="flex items-center space-x-0.5">
            <svg className="h-4" data-info="Icon by Free Preloaders on freeicons.io" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
              <path d="M8.333 2.5h3.334a6.667 6.667 0 0 1 0 13.333v2.917c-4.167-1.667-10-4.167-10-9.583A6.667 6.667 0 0 1 8.333 2.5ZM10 14.167h1.667a5.001 5.001 0 0 0 0-10H8.333a5 5 0 0 0-5 5c0 3.008 2.052 4.971 6.667 7.066v-2.066Z" fill="#788EA5" />
            </svg>
            <span className="text-cardTag ">
              {comments}
            </span>
          </div>
        </div>

      </div>

    </div>
  );
}

export default Card;
