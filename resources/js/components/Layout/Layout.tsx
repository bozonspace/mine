import React from 'react';
import { Outlet } from 'react-router-dom';
import Footer from './Footer';
import Header from './Header/Header';

function Layout() {
  return (
    <>
      <Header />
      <main className="pb-20 min-h-180">
        <Outlet />
      </main>
      <Footer />
    </>
  );
}

export default Layout;
