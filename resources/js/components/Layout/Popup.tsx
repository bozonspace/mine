import React, { MouseEventHandler, useState } from 'react';

import SecondaryButton from '../Buttons/SecondaryButton';
import SubmitButton from '../Buttons/SubmitButton';

interface PopupI {
    title: string,
    onSubmit: MouseEventHandler<HTMLButtonElement>,
    onCancel: MouseEventHandler<HTMLButtonElement>,
    updating?: boolean,
}
function Popup(props:PopupI) {
  const {
    onSubmit, title, onCancel, updating,
  } = props;
  return (
    <div className="w-full h-full z-10 fixed top-0">
      <div className="bg-white w-full h-full flex justify-center items-center">
        <div className="rounded-lg p-5 shadow-lg min-w-128">
          <div className="flex justify-between gap-2">
            <h2 className="text-h2 font-semibold">
              {title}
            </h2>
            <button type="button" onClick={onCancel}>
              <svg className="w-6 h-6" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14">
                <path d="M7 .953A6.055 6.055 0 0 0 .953 7c.332 8.021 11.762 8.018 12.094 0A6.056 6.056 0 0 0 7 .953Zm2.897 8.348a.422.422 0 0 1-.596.596L7 7.597l-2.3 2.3a.422.422 0 0 1-.597-.596L6.403 7l-2.3-2.3a.422.422 0 1 1 .596-.597L7 6.403l2.3-2.3a.422.422 0 0 1 .597.596L7.597 7l2.3 2.3Z" fill="#394756" />
              </svg>
            </button>

          </div>
          <div className="flex gap-3">
            <SecondaryButton
              text="Cancel"
              className="w-30 mt-5"
              callback={onCancel}
              disabled={updating}
            />
            <SubmitButton
              text="Submit"
              className="w-30 mt-5"
              type="submit"
              callback={onSubmit}
              disabled={updating}
            />
          </div>

        </div>
      </div>

    </div>
  );
}

Popup.defaultProps = {
  updating: false,
};

export default Popup;
