import React, { useEffect, useState } from 'react';

interface InputProps {
    label?: string;
    value?: string;
    placeholder?: string;
    error?: boolean,
    errorText?: string,
    type?: 'text' | 'number' | 'email' | 'password',
    setter: Function,
    className?: string,
    size?: number,
}

function Input(props: InputProps) {
  const {
    label, value, error, placeholder, type, setter, className, size, errorText,
  } = props;
  const [val, setVal] = useState<string>(value);
  useEffect(() => {
    setVal(value);
  }, [value]);
  const onChangeInput = (e:React.BaseSyntheticEvent) => {
    const v = e.currentTarget.value;
    setVal(v);
    setter(v);
  };
  return (
    <div className={`${className} ${error ? 'text-orange-60' : ''}`}>
      <div className="text-cardTag py-1">{label}</div>
      <input
        className={`rounded-lg border-1 p-2 w-full ${error ? 'border-pink-50' : ''}`}
        placeholder={placeholder}
        value={val}
        type={type}
        onChange={onChangeInput}
        size={size}
        name={label}
      />
      <div className="text-cardTag py-1 text-right opacity-80">{errorText}</div>
    </div>
  );
}
Input.defaultProps = {
  label: '',
  value: '',
  className: '',
  placeholder: '',
  errorText: '',
  error: false,
  type: 'text',
  size: 100,
};

export default Input;
