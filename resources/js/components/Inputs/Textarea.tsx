import React, { useEffect, useState } from 'react';

interface TextareaProps {
    label?: string;
    value?: string;
    placeholder?: string;
    error?: boolean,
    errorText?: string,
    type?: 'text' | 'number' | 'email' | 'password',
    setter: Function,
    className?: string,
    maxLength?: number,
    rows?: number,
    cols?: number,
}

function Textarea(props: TextareaProps) {
  const {
    label, value, error, placeholder, type, setter, className, maxLength, errorText,
    rows, cols,
  } = props;
  const [val, setVal] = useState<string>(value);
  const [quantity, setQuantity] = useState<number>(150);
  useEffect(() => {
    setVal(value);
  }, [value]);
  const onChangeInput = (e:React.BaseSyntheticEvent) => {
    const v = e.currentTarget.value;
    setVal(v);
    setter(v);
    setQuantity(maxLength - v.length);
  };
  return (
    <div className={`${className} ${error ? 'text-pink-50' : ''}`}>
      <div className="text-cardTag py-1">{label}</div>
      <textarea
        className={`rounded-lg border-1 p-2 w-full ${error ? 'border-pink-50' : ''}`}
        placeholder={placeholder}
        value={val}
        onChange={onChangeInput}
        maxLength={maxLength}
        rows={rows}
        cols={cols}
        name={label}
      />
      <div className={`text-cardTag py-1 text-right opacity-90 ${quantity < 10 ? 'text-pink-50' : ''}`}>
        {`Available characters: ${quantity}`}
      </div>
      <div className="text-cardTag py-1 text-right opacity-80">{errorText}</div>
    </div>
  );
}
Textarea.defaultProps = {
  label: '',
  value: '',
  className: '',
  placeholder: '',
  errorText: '',
  error: false,
  type: 'text',
  maxLength: 100,
  rows: 3,
  cols: 5,
};

export default Textarea;
