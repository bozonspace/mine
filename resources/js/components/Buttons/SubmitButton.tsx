import React, { MouseEventHandler } from 'react';

interface SubmitButtonProps {
    text: string;
    callback?: MouseEventHandler<HTMLButtonElement>,
    className?: string,
    type?: 'submit' | 'button'
    disabled?: boolean,
}

function SubmitButton(props: SubmitButtonProps) {
  const {
    text, callback, className, type, disabled,
  } = props;
  return (
    <button
      // eslint-disable-next-line react/button-has-type
      type={type}
      className={`${className} rounded-lg text-center font-semibold text-white p-2 bg-blue-30 hover:bg-blue-40 disabled:bg-darkGray-20 disabled:text-darkGray-50`}
      onClick={callback}
      disabled={disabled}
    >
      {text}
    </button>
  );
}
SubmitButton.defaultProps = {
  className: '',
  callback: null,
  type: 'button',
  disabled: false,
};

export default SubmitButton;
