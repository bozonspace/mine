import React, { MouseEventHandler } from 'react';

interface SecondaryButtonProps {
    text: string;
    callback?: MouseEventHandler<HTMLButtonElement>,
    className?: string,
    disabled?: boolean,
}

function SecondaryButton(props: SecondaryButtonProps) {
  const {
    text, callback, className, disabled,
  } = props;
  return (
    <button
      type="button"
      className={`${className} rounded-lg text-center font-semibold text-darkGray-50 p-2 bg-blue-5 hover:bg-blue-10 disabled:bg-darkGray-5 disabled:text-darkGray-30`}
      onClick={callback}
      disabled={disabled}
    >
      {text}
    </button>
  );
}
SecondaryButton.defaultProps = {
  className: '',
  callback: null,
  disabled: false,
};

export default SecondaryButton;
