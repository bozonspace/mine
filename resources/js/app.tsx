import React, { useEffect } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Layout from './components/Layout/Layout';
import Login from './pages/EntryPages/Login';
import NothingFound from './pages/NothingFound';
import Post from './pages/Post';
import Posts from './pages/Posts';
import Register from './pages/EntryPages/Register';
import RegisterCheckEmail from './pages/EntryPages/RegisterCheckEmail';
import { useAppDispatch } from './redux/hooks';
import { loginAsync } from './redux/slicers/userSlice';
import RegisterCheckEmailSuccess from './pages/EntryPages/RegisterCheckEmailSuccess';
import PostCreate from './pages/PostCreate';
import PostEdit from './pages/PostEdit';

function App() {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(loginAsync());
  }, []);
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Posts />} />
          <Route path="post">
            <Route path="create" element={<PostCreate />} />
            <Route path="edit/:id" element={<PostEdit />} />
            <Route path=":id" element={<Post />} />
          </Route>
          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} />
          <Route path="register-check-email/:email" element={<RegisterCheckEmail />} />
          <Route path="register-check-email-success/:email" element={<RegisterCheckEmailSuccess />} />
          <Route path="*" element={<NothingFound />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
