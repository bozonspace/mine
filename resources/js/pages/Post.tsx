import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';

import Comment from '../components/Comment';
import Popup from '../components/Layout/Popup';

import Search from '../components/Search/Search';
import { makeid } from '../helpers/helpers';
import { CommentI, TagI } from '../helpers/interfaces';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { getPostAsync, selectPost } from '../redux/slicers/postsSlice';
import { selectUser } from '../redux/slicers/userSlice';

function Post() {
  const dispatch = useAppDispatch();

  const { id } = useParams();
  const post = useAppSelector(selectPost);
  const userRedux = useAppSelector(selectUser);
  const {
    title, tags, bookmark, user, views, comments, updatedAt,
  } = post;

  const [openedPopup, setOpenedPopup] = useState<boolean>(false);
  const [updating, setUpdating] = useState<boolean>(false);

  useEffect(() => {
    dispatch(getPostAsync(Number(id)));
  }, []);

  const deletePost = (e: React.ChangeEvent<any>) => {
    e.preventDefault();
    setUpdating(true);
    axios.post('/api/post-delete', {
      postId: id,
    }).then((res) => {
      setUpdating(false);
      if (res.data.success) {
        window.location = res.data.path;
      }
      return false;
      // TODO: handle error
    })
      .catch((error) => {
        setUpdating(false);
        console.log(error);
      });
  };

  return (
    <div id="Post">
      {
        openedPopup ? (
          <Popup
            title="Confirm deletion?"
            onSubmit={deletePost}
            onCancel={() => setOpenedPopup(false)}
          />
        ) : ''
      }
      <div className="flex items-center justify-center mt-3">
        <Search />
      </div>

      <div className="flex flex-wrap gap-5 mt-7">
        <div className="min-w-64 flex-none" />
        {
            post ? (
              <div className="w-64 flex-1">
                <div className="flex flex-wrap justify-between text-cardTag text-darkGray-60">
                  <div className="flex flex-wrap items-center gap-1">
                    <div className="w-6 h-6">
                      <img className="rounded-full w-full h-full" src="/assets/placeholder.png" alt="" />
                    </div>
                    <Link to={`/?search=@${user?.nickname}`}>
                      <span className="hover:underline">{user?.nickname}</span>
                    </Link>
                  </div>
                  <div className="flex items-center space-x-2.5">
                    {
                      user?.id === userRedux?.id ? (
                        <div className="flex gap-4">
                          <button
                            onClick={() => setOpenedPopup(true)}
                            type="button"
                            className="bg-pink-5 rounded p-1 border-1 border-darkGray-10 hover:bg-pink-20 hover:text-black"
                          >
                            Delete
                          </button>
                          <Link to={`/post/edit/${id}`}>
                            <div className="rounded p-1 border-1 border-darkGray-10 hover:bg-darkGray-10 hover:text-black">
                              Edit
                            </div>
                          </Link>
                        </div>
                      ) : ''
                    }
                    <div className=" text-left">
                      {updatedAt}
                    </div>
                    <div className="flex items-center space-x-0.5">
                      <svg className="h-4" data-info="Icon by Raj Dev on freeicons.io" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 21">
                        <path d="M16.625 18.375 10.5 14l-6.125 4.375v-14a1.75 1.75 0 0 1 1.75-1.75h8.75a1.75 1.75 0 0 1 1.75 1.75v14Z" stroke="#788EA5" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                      </svg>
                      <span className="">
                        {bookmark}
                      </span>
                    </div>
                    <div className="flex items-center space-x-0.5">
                      <svg className="h-4" data-info="Icon by Raj Dev on freeicons.io" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <g clipPath="url(#a)" stroke="#788EA5" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                          <path d="M.833 10S4.167 3.333 10 3.333 19.167 10 19.167 10 15.833 16.667 10 16.667.833 10 .833 10Z" />
                          <path d="M10 12.5a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z" />
                        </g>
                        <defs><clipPath id="a"><path fill="#fff" d="M0 0h20v20H0z" /></clipPath></defs>
                      </svg>
                      <span className="">
                        {views}
                      </span>
                    </div>
                    <div className="flex items-center space-x-0.5">
                      <svg className="h-4" data-info="Icon by Free Preloaders on freeicons.io" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M8.333 2.5h3.334a6.667 6.667 0 0 1 0 13.333v2.917c-4.167-1.667-10-4.167-10-9.583A6.667 6.667 0 0 1 8.333 2.5ZM10 14.167h1.667a5.001 5.001 0 0 0 0-10H8.333a5 5 0 0 0-5 5c0 3.008 2.052 4.971 6.667 7.066v-2.066Z" fill="#788EA5" />
                      </svg>
                      <span className="">
                        {comments?.length}
                      </span>
                    </div>
                  </div>
                </div>
                <h1 className={`mt-2 font-semibold ${title?.length >= 50 ? 'text-h2' : 'text-h1'}`}>
                  {title}
                </h1>
                <div className="flex flex-wrap gap-3 mt-4">
                  {
                  Array.isArray(tags) ? tags.map((e:TagI) => (
                    <Link to={`/?search=#${encodeURI(e.tag)}`} key={makeid(10)}>
                      <div className="rounded bg-darkGray-10 flex font-semibold p-1 text-darkGray-60" key={e.id}>
                        {e.tag}
                      </div>
                    </Link>
                  )) : ''
                  }
                </div>
                <div className="mt-5 flex flex-col gap-2">
                  {
                    Array.isArray(comments) && comments.length ? comments.map((e:CommentI) => (
                      <Comment cmt={e} key={makeid(10)} />
                    ))
                      : (
                        <div className="text-center mt-5">
                          No comments yet
                        </div>
                      )
                  }
                </div>
              </div>
            ) : ''
        }
        <div className="min-w-64 flex-none" />
      </div>

    </div>
  );
}

export default Post;
