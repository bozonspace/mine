import axios from 'axios';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import { Tag, WithContext as ReactTags } from 'react-tag-input';
import SecondaryButton from '../components/Buttons/SecondaryButton';
import SubmitButton from '../components/Buttons/SubmitButton';
import Textarea from '../components/Inputs/Textarea';

const TAGS:Array<string> = [];

const suggestions = TAGS.map((t) => ({
  id: t,
  text: t,
}));

const KeyCodes = {
  comma: 188,
  enter: 13,
};

const delimiters = [KeyCodes.comma, KeyCodes.enter];

function PostCreate() {
  const [text, setText] = useState<string>('');
  const [updating, setUpdating] = useState<boolean>(false);
  const [errorMes, setErrorMes] = useState<string>('');
  const [tags, setTags] = useState<Array<Tag>>([]);

  const handleDelete = (i:number) => {
    setTags(tags.filter((tag, index) => index !== i));
  };

  const handleAddition = (tag:Tag) => {
    if (tags.length < 10) { setTags([...tags, tag]); }
  };

  const handleDrag = (tag:Tag, currPos:number, newPos:number) => {
    const newTags = tags.slice();

    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);

    // re-render
    setTags(newTags);
  };

  const handleTagClick = (index:number) => {
    // console.log(`The tag at index ${index} was clicked`);
  };

  const submit = (e: React.ChangeEvent<any>) => {
    e.preventDefault();
    setUpdating(true);
    axios.post('/api/post-create', {
      text,
      tags,
    }).then((res) => {
      setUpdating(false);
      if (res.data.success) {
        window.location = res.data.path;
      }
      return false;
      // TODO: handle error
    })
      .catch((error) => {
        setUpdating(false);
        if (error.response.data?.errors?.text) {
          setErrorMes(error.response.data.errors.text);
        } else if (error.response.data?.errors?.tags) {
          setErrorMes(error.response.data.errors.tags);
        }
      });
  };

  return (
    <div className="mt-10 ml-60 w-136">
      <h1 className="mt-2 font-semibold text-h1">
        Start a discussion
      </h1>
      <form onSubmit={submit} className="mt-5">
        <Textarea
          value={text}
          placeholder="Type something interesting..."
          errorText=""
          error={false}
          type="email"
          setter={setText}
          className="w-full"
          maxLength={150}
        />
        <div>
          <ReactTags
            tags={tags}
            suggestions={suggestions}
            delimiters={delimiters}
            handleDelete={handleDelete}
            handleAddition={handleAddition}
            handleDrag={handleDrag}
            handleTagClick={handleTagClick}
            inputFieldPosition="bottom"
            autocomplete
            placeholder="Input at least one tag and press Enter"
            autofocus={false}
          />
          <div className={`text-cardTag py-1 text-right opacity-90 ${10 - tags.length < 4 ? 'text-pink-50' : ''}`}>
            {`Available tags: ${10 - tags.length}`}
          </div>
        </div>

        <div className="flex space-x-3 w-full justify-end">
          <Link to="/">
            <SecondaryButton
              text="Cancel"
              className="w-30 mt-5"
            />
          </Link>
          <SubmitButton
            text="Publish"
            className="w-30 mt-5"
            type="submit"
            disabled={updating}
          />
        </div>

      </form>
      <div className="mt-1 text-cardTag py-1 text-pink-50 text-right">
        {errorMes}
      </div>
    </div>
  );
}

export default PostCreate;
