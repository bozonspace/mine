import React from 'react';
import { Link, useParams } from 'react-router-dom';
import SubmitButton from '../../components/Buttons/SubmitButton';

function RegisterCheckEmailSuccess() {
  const { email } = useParams();
  return (
    <div className="mt-10 ml-60 w-96">
      <h1 className="mt-2 font-semibold text-h1">
        Registration
      </h1>
      <div className="text-h2 mt-5 text-darkGray-50">
        {`You have successfully registered! Please login with ${email}.`}
      </div>
      <Link to="/login">
        <SubmitButton
          text="Login"
          className="w-60 mt-5"
          type="button"
        />
      </Link>
    </div>
  );
}

export default RegisterCheckEmailSuccess;
