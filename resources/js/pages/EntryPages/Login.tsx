import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import Input from '../../components/Inputs/Input';
import SubmitButton from '../../components/Buttons/SubmitButton';
import SecondaryButton from '../../components/Buttons/SecondaryButton';

function Login() {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [errorMes, setErrorMes] = useState<string>('');
  const [updating, setUpdating] = useState<boolean>(false);

  const submit = (e: React.ChangeEvent<any>) => {
    e.preventDefault();
    setUpdating(true);
    axios.post('/api/login', {
      email,
      password,
    }).then((res) => {
      setUpdating(false);
      if (res.data.success) {
        const path = document.referrer.includes('login') || document.referrer === '' ? '/' : document.referrer;
        window.location.href = path;
        //  window.location = res.data.path;
      }
      return false;
    })
      .catch((error) => {
        setUpdating(false);
        setErrorMes(error.response.data.message);
      });
  };

  return (
    <div className="mt-10 ml-60 w-60">
      <h1 className="mt-2 font-semibold text-h1">
        Login
      </h1>
      <form onSubmit={submit}>
        <Input
          value={email}
          placeholder="dummy@mail.com"
          label="email"
          errorText=""
          error={false}
          type="email"
          setter={setEmail}
          className="w-full mt-5"
        />
        <Input
          value={password}
          placeholder=""
          label="password"
          errorText=""
          error={false}
          type="password"
          setter={setPassword}
          className="w-60 mt-2"
        />
        <SubmitButton
          text="Login"
          className="w-60 mt-5"
          type="submit"
          disabled={updating}
        />
        <div className="mt-1 text-cardTag py-1 text-pink-50">
          {errorMes}
        </div>
      </form>
      <SecondaryButton
        text="Forgot password"
        className="w-60 mt-5"
      />
      <div className="w-60 py-3 flex justify-between gap-2 text-cardTag">
        <span>-----------</span>
        <span>or</span>
        <span>-----------</span>
      </div>
      <Link to="/register">
        <button type="button" className="w-60 text-center font-semibold text-darkGray-50">
          Register
        </button>
      </Link>
    </div>
  );
}

export default Login;
