import React from 'react';
import { useParams } from 'react-router-dom';

function RegisterCheckEmail() {
  const { email } = useParams();
  return (
    <div className="mt-10 ml-60 w-96">
      <h1 className="mt-2 font-semibold text-h1">
        Registration
      </h1>
      <div className="text-h2 mt-5 text-darkGray-50">
        {`We’ve just sent you email on ${email}. Please check it for confirmation link.`}
      </div>
    </div>
  );
}

export default RegisterCheckEmail;
