import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Input from '../../components/Inputs/Input';
import SubmitButton from '../../components/Buttons/SubmitButton';

function Register() {
  const [email, setEmail] = useState<string>('');
  const [nickname, setNickname] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [passwordConf, setPasswordConf] = useState<string>('');
  const [updating, setUpdating] = useState<boolean>(false);
  const [errorMes, setErrorMes] = useState<string>('');

  const submit = (e: React.ChangeEvent<any>) => {
    e.preventDefault();
    setUpdating(true);
    axios.post('/api/register', {
      nickname,
      email,
      password,
      password_confirmation: passwordConf,
    }).then((res) => {
      setUpdating(false);
      if (res.data.success) {
        window.location = res.data.path;
      }
      return false;
      // TODO: handle error
    })
      .catch((error) => {
        setUpdating(false);
        if (error.response.data?.errors?.password) {
          setErrorMes(error.response.data.errors.password);
        } else if (error.response.data?.errors?.nickname) {
          setErrorMes(error.response.data.errors.nickname);
        } else if (error.response.data?.errors?.email) {
          setErrorMes(error.response.data.errors.email);
        } else setErrorMes(error.response.data.message);
      });
  };

  return (
    <div className="mt-10 ml-60 w-60">
      <h1 className="mt-2 font-semibold text-h1">
        Registration
      </h1>
      <form onSubmit={submit} className="mt-5">
        <Input
          value={email}
          placeholder="dummy@mail.com"
          label="email"
          errorText=""
          error={false}
          type="email"
          setter={setEmail}
          className="w-full"
        />
        <Input
          value={nickname}
          placeholder="dummy"
          label="nickname"
          errorText=""
          error={false}
          type="text"
          setter={setNickname}
          className="w-60 mt-2"
          size={10}
        />
        <Input
          value={password}
          placeholder=""
          label="password"
          errorText=""
          error={false}
          type="password"
          setter={setPassword}
          className="w-60 mt-2"
        />
        <Input
          value={passwordConf}
          placeholder=""
          label="password confirmation"
          errorText=""
          error={false}
          type="password"
          setter={setPasswordConf}
          className="w-60 mt-2"
        />
        <SubmitButton
          text="Register"
          className="w-60 mt-5"
          type="submit"
          disabled={updating}
        />
      </form>
      <div className="mt-1 text-cardTag py-1 text-orange-60">
        {errorMes}
      </div>

      <div className="w-60 py-3 flex justify-between gap-2 text-cardTag">
        <span>-----------</span>
        <span>or</span>
        <span>-----------</span>
      </div>
      <Link to="/login">
        <button type="button" className="w-60 text-center font-semibold text-darkGray-50">
          Login
        </button>
      </Link>
    </div>
  );
}

export default Register;
