import React, { useEffect, useState } from 'react';
import Masonry from 'react-masonry-css';
import { useLocation } from 'react-router-dom';

import Card from '../components/Layout/Card';
import Search from '../components/Search/Search';
import { makeid } from '../helpers/helpers';
import { CardI } from '../helpers/interfaces';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { selectPosts, getPostsAsync, getPostsSearchAsync } from '../redux/slicers/postsSlice';

// function useQuery() {
//   const { search } = useLocation();
//   return React.useMemo(() => new URLSearchParams(search), [search]);
// }


function Posts() {
  const dispatch = useAppDispatch();
  const location = useLocation();
  const cards = useAppSelector(selectPosts);
  const [offset, setOffset] = useState<number>(0);
  const [search, setSearch] = useState<string>('');

  const invokeDispatch = (searchLoc: string = '') => {
    const thunkProps = {
      offset,
      search: searchLoc,
    };
    dispatch(getPostsSearchAsync(thunkProps));
    // dispatch(getPostsAsync(thunkProps));
  };

  const getParamsFromURL = () => {
    const { href, origin } = window.location;
    let query = href.replace(origin, '');
    query = query.replace('/?', '');
    const params = query.split('&').reduce((res:any, item) => {
      const parts = item.split('=');
      res[parts[0]] = decodeURI(parts[1]);
      return res;
    }, {});
    invokeDispatch(params?.search);
    setSearch(params?.search);
  };

  useEffect(() => {
    getParamsFromURL();
  }, [location]);

  const breakpointColumnsObj = {
    default: 4,
    1100: 3,
    700: 2,
    500: 1,
  };
  return (
    <div id="PostsPage">
      <div className="flex items-center justify-center mt-3">
        <Search
          text={search}
        />
      </div>
      {
      Array.isArray(cards) && cards.length
        ? (
          <Masonry
            breakpointCols={breakpointColumnsObj}
            className="my-masonry-grid"
            columnClassName="my-masonry-grid_column"
          >
            {
              cards.map((e:CardI) => (<Card card={e} key={makeid(10)} />))
            }
          </Masonry>
        )
        : (
          <div className="text-center mt-5">
            No posts yet
          </div>
        )
      }
    </div>
  );
}

export default Posts;
