export interface TagI {
    tag: string,
    id: number,
}

export interface CardI {
    title: string,
    tags: Array<TagI>,
    id: number,
    bookmark: number,
    views: number,
    comments: number,
    updatedAt: string,
}
export interface UserI {
    id: number,
    nickname: string,
    email?: string,
}
export interface CommentI {
    id: number,
    comment: string,
    postId: number,
    userId: number,
    user?: UserI,
    updatedAt: string,
}
export interface PostI {
    title?: string,
    id?: number,
    userId?: number,
    updatedAt?: string,
    bookmark?: number,
    tags?: Array<TagI>,
    user?: UserI,
    views?: number,
    comments?: Array<CommentI>,
}

export interface ThunkI {
    offset: number,
    search: string
  }
