export function DeleteFields(obj, fields) {
  const objCopy = { ...obj };
  fields.forEach((field) => {
    delete objCopy[field];
  });

  return objCopy;
}

export function makeid(length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i += 1) {
    result += characters.charAt(Math.floor(Math.random()
 * charactersLength));
  }
  return result;
}

export const shortenIt = (str, num) => {
  if (num && typeof num === 'number' && typeof str === 'string') {
    return str.length > num ? `${str.substr(0, num)}...` : str;
  }
  return str;
};
