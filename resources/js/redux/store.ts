import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import postsReducer from './slicers/postsSlice';
import userReducer from './slicers/userSlice';

export const store = configureStore({
  reducer: {
    postsReducer,
    userReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
