import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  UserI,
} from '../../helpers/interfaces';
/* eslint-disable no-param-reassign */
import { RootState } from '../store';
import { loginUser } from '../api/userAPI';

export interface userState {
  user: UserI,
}

const initialState: userState = {
  user: {
    id: 0,
    nickname: '',
    email: '',
  },
};

export const loginAsync = createAsyncThunk(
  'user/getMe',
  async () => {
    const response = await loginUser();
    return response.data;
  },
);

export const userSlice = createSlice({
  name: 'userSlice',
  initialState,
  reducers: {
    setUser: (state, action: PayloadAction<UserI>) => {
      state.user = action.payload;
    },
  },

  extraReducers: (builder) => {
    builder
      .addCase(loginAsync.fulfilled, (state, action) => {
        state.user = action.payload;
      });
  },
});

export const { setUser } = userSlice.actions;

export const selectUser = (state: RootState) => state.userReducer.user;

export default userSlice.reducer;
