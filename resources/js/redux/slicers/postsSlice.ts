import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

/* eslint-disable no-param-reassign */
import { RootState } from '../store';
import { fetchPosts, fetchPost } from '../api/postAPI';
import { ThunkI, PostI, CardI } from '../../helpers/interfaces';

export interface postsState {
  posts: Array<CardI>,
  post: PostI
}

const initialState: postsState = {
  posts: [],
  post: {},
};

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched. Thunks are
// typically used to make async requests.
export const getPostsAsync = createAsyncThunk(
  'posts/fetchPosts',
  async (thunkProps:ThunkI) => {
    const response = await fetchPosts(thunkProps);
    // The value we return becomes the `fulfilled` action payload
    return response.data;
  },
);

export const getPostsSearchAsync = createAsyncThunk(
  'posts/fetchPostsSearch',
  async (thunkProps:ThunkI) => {
    const response = await fetchPosts(thunkProps);
    // The value we return becomes the `fulfilled` action payload
    return response.data;
  },
);

export const getPostAsync = createAsyncThunk(
  'post/fetchPost',
  async (postId: number = 0) => {
    const response = await fetchPost(postId);
    // The value we return becomes the `fulfilled` action payload
    return response.data;
  },
);

export const postsSlice = createSlice({
  name: 'postsSlice',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    // increment: (state) => {
    // Redux Toolkit allows us to write "mutating" logic in reducers. It
    // doesn't actually mutate the state because it uses the Immer library,
    // which detects changes to a "draft state" and produces a brand new
    // immutable state based off those changes
    // state.value += 1;
    // },

    // Use the PayloadAction type to declare the contents of `action.payload`
    setPosts: (state, action: PayloadAction<Array<CardI>>) => {
      state.posts = action.payload;
    },
    setPost: (state, action: PayloadAction<PostI>) => {
      state.post = action.payload;
    },
  },
  // The `extraReducers` field lets the slice handle actions defined elsewhere,
  // including actions generated by createAsyncThunk or in other slices.
  extraReducers: (builder) => {
    builder
      // .addCase(getPostsAsync.pending, (state) => {
      //   state.status = 'loading';
      // })
      .addCase(getPostsAsync.fulfilled, (state, action) => {
        state.posts = state.posts.concat(action.payload);
      })
      .addCase(getPostsSearchAsync.fulfilled, (state, action) => {
        state.posts = action.payload;
      })
      .addCase(getPostAsync.fulfilled, (state, action) => {
        state.post = { ...state.post, ...action.payload };
      });
  },
});

export const { setPosts } = postsSlice.actions;
export const { setPost } = postsSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectPosts = (state: RootState) => state.postsReducer.posts;
export const selectPost = (state: RootState) => state.postsReducer.post;

// We can also write thunks by hand, which may contain both sync and async logic.
// Here's an example of conditionally dispatching actions based on current state.
// export const incrementIfOdd = (amount: number): AppThunk => (
//   dispatch,
//   getState,
// ) => {
//   const currentValue = selectCount(getState());
//   if (currentValue % 2 === 1) {
//     dispatch(incrementByAmount(amount));
//   }
// };

export default postsSlice.reducer;
