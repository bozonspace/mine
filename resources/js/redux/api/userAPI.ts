import axios from 'axios';


export function loginUser() {
  return axios.get('/api/get-me', {})
    .then((res) => {
      if (res.data.success) {
        return res.data;
      }
      return false;
    // TODO: handle error
    })
    .catch((error) => {
      if (error.response.status === 401) {
        // redirect to login
      }
    });
}
