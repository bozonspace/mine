import axios from 'axios';
import { ThunkI } from '../../helpers/interfaces';

export function fetchPosts(thunkProps:ThunkI) {
  const { offset, search } = thunkProps;
  return axios.get('/api/get-posts', {
    params: {
      offset,
      search,
    },
  }).then((res) => {
    if (res.data.success) {
      return res.data;
    }
    return false;
    // TODO: handle error
  });
}

export function fetchPost(postId: number) {
  return axios.get('/api/get-post', {
    params: {
      postId,
    },
  }).then((res) => {
    if (res.data.success) {
      return res.data;
    }
    return false;
    // TODO: handle error
  });
}
