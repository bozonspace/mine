<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsAndLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_and_likes', function (Blueprint $table) {
            $table->id();
            $table->enum('value', [1, -1]);
            $table->unsignedBigInteger('post_id')->nullable();
            $table->timestamps();

            $table->foreign('post_id')->references('id')->on('posts')->constrained()->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts_and_likes');
    }
}
