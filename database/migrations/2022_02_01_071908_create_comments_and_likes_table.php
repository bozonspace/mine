<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsAndLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments_and_likes', function (Blueprint $table) {
            $table->id();
            $table->enum('value', [1, -1]);
            $table->unsignedBigInteger('comment_id')->nullable();
            $table->timestamps();

            $table->foreign('comment_id')->references('id')->on('comments')->constrained()->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments_and_likes');
    }
}
