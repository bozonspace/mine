<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            RoleSeeder::class,
        ]);

         \App\Models\User::factory(10)->create();
         \App\Models\Post::factory(10)->create();
         \App\Models\Tag::factory(10)->create();
         \App\Models\TagAndPost::factory(50)->create();
         \App\Models\Comment::factory(50)->create();
    }
}
