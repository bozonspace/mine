<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = \Config::get('constants.roles');
        
        foreach ($roles as $key => $value) {
            DB::table("roles")->insert([
                'id' => $value,
                'role' => $key,
            ]);
        }
    }
}
