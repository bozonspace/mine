<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->insert([
            "nickname" => "maestro",
            "email" => "admin@email.com",
            "password" => bcrypt('password'),
            "role_id" => 2
        ]);
    }
}
