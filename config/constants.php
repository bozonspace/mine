<?php

return [
    'roles' => [
        "admin"      => 1,
        "registered" => 2,
        "moderator"  => 3,
    ],
];