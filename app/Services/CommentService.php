<?php

namespace App\Services;

use App\Models\Comment;
use Carbon\Carbon;

class CommentService
{
    function __construct()
    {
    }

    /**
     * Get Comments By Post Id
     *
    */
    public function getCommentsByPostId($postId)
    {
        $postId = (int)$postId;
        $comments = Comment::select('id', 'comment', 'post_id as postId', 'user_id as userId', 'updated_at as updatedAt', 'created_at as createdAt')->where('post_id', '=', $postId)->get();
        $userService = new UserService();
        foreach($comments as $c) {
            $c->updatedAt =(new Carbon($c->updatedAt))->diffForHumans();
            $c->user = ($userService->getUser($c->userId))['data'];
        }
        return [
            'data' => $comments,
            'success' => true,
            'message' => ''
        ];
    }    
}
