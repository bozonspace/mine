<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class TagService
{
    function __construct()
    {
    }

    /**
    * Get Tags By Post Id
    *
    **/
    public function getTagsByPostId($postId)
    {
        $postId = (int)$postId;
        $tags = DB::select( DB::raw("SELECT t.id, t.tag
        FROM tag_and_posts tap
        LEFT JOIN tags t ON tap.tag_id = t.id
        WHERE tap.post_id = $postId
        GROUP BY t.id"));
        return [
            'data' => $tags,
            'success' => true,
            'message' => ''
        ];
    }    
}
