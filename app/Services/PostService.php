<?php

namespace App\Services;

use App\Models\Post;
use App\Models\Tag;
use App\Models\TagAndPost;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class PostService
{
    function __construct()
    {
    }

    /**
     * Get Posts API
     *
    */
    public function getPosts($offset, $search = '')
    {
        $offset = $offset || 0;
        $limit = 20;
        $search = trim($search);
        $searchBy = $search ? $search[0] : '';
        $whereCond = '';
        $joinCond = '';
        $bindings = [
            'offset' => $offset,
            'limit' => $limit,
        ];
        if($search && strlen($search) >= 3) {
            switch($searchBy) {
                case '@': 
                    $joinCond = "LEFT JOIN users u ON u.id = p.user_id";
                    $whereCond = "WHERE u.nickname LIKE :search";
                    $bindings = array_merge($bindings, ['search' => '%'.substr($search, 1).'%']);
                    break;
                case '#': 
                    $joinCond = "LEFT JOIN tag_and_posts tap ON tap.post_id = p.id ";
                    $joinCond .= "LEFT JOIN tags t ON tap.tag_id = t.id ";
                    $whereCond = "WHERE t.tag LIKE :search";
                    $bindings = array_merge($bindings, ['search' => '%'.substr($search, 1).'%']);
                    break;
                default: 
                    $whereCond = "WHERE p.title LIKE :search";
                    $bindings = array_merge($bindings, ['search' => '%'.$search.'%']);
                    break;
            };
        }
        // DB::enableQueryLog();
        $posts = DB::select( DB::raw("SELECT p.id, p.title, p.updated_at as updatedAt
                    FROM posts p
                    $joinCond
                    $whereCond
                    ORDER BY p.created_at DESC
                    LIMIT :limit
                    OFFSET :offset"),
                    $bindings
                );
        // dd(DB::getQueryLog());
        $commentService = new CommentService();
        $tagService = new TagService();
        foreach($posts as $p) {
            $p->comments = count(($commentService->getCommentsByPostId($p->id))['data']);
            $p->tags = ($tagService->getTagsByPostId($p->id))['data'];
            $p->views = 0;
            $p->bookmark = 0;
            $p->updatedAt = (new Carbon($p->updatedAt))->diffForHumans();
        }
        
        return json_encode([
            'data' => $posts,
            'success' => true,
            'message' => ''
        ]);
    }

    /**
     * Get Post API
     *
    */
    public function getPost($postId)
    {
        $postId = (int)$postId;
        $post = Post::where('id', '=', $postId)->select('id', 'title', 'user_id as userId', 'updated_at as updatedAt')->first();
        $commentService = new CommentService();
        $tagService = new TagService();
        $userService = new UserService();
        if($post) {
            $post->comments = ($commentService->getCommentsByPostId($post->id))['data'];
            $post->tags =($tagService->getTagsByPostId($post->id))['data'];
            $post->views = 0;
            $post->bookmark = 0;
            $post->updatedAt = (new Carbon($post->updatedAt))->diffForHumans();
            $post->user = ($userService->getUser($post->userId))['data'];
        }
        
        return json_encode([
            'data' => $post,
            'success' => true,
            'message' => ''
        ]);
    }

    /**
     * Create a post API
     *
    */
    public function postCreate($userId, $tags, $text)
    {
        $post = Post::create([
            'title' => $text,
            'user_id' => $userId
        ]);
        foreach($tags as $tag) {
            $t = Tag::firstOrCreate([
                'tag' => $tag['text'],
            ]);
            TagAndPost::create([
                'tag_id' => $t->id,
                'post_id' => $post->id
            ]);
        };
        return json_encode([
            'data' => $post,
            'success' => true,
            'message' => 'Post created'
        ]);
    }

    /**
     * Update a post API
     *
    */
    public function postUpdate($userId, $tags, $text, $postId)
    {
        $post = Post::where([
            ['id', '=', $postId],
            ['user_id', '=', $userId],
        ])->update([
            'title' => $text,
        ]);
        TagAndPost::where('post_id',$postId)->delete();
        foreach($tags as $tag) {
            $t = Tag::firstOrCreate([
                'tag' => $tag['text'],
            ]);
            TagAndPost::firstOrCreate([
                'tag_id' => $t->id,
                'post_id' => $postId
            ]);
        };
        return json_encode([
            'data' => '',
            'success' => true,
            'message' => 'Post updated'
        ]);
    }
    
}
