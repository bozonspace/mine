<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserService
{
    function __construct()
    {
    }

    /**
    * Get User By Id
    *
    **/
    public function getUser($userId)
    {
        $userId = (int)$userId;
        $user = User::find($userId);
        return [
            'data' => $user,
            'success' => true,
            'message' => ''
        ];
    }    
}
