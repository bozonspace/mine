<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TagAndPost extends Model
{
    use HasFactory;

    protected $fillable = [
        'tag_id',
        'post_id',
    ];
    
    protected $hidden = ["created_at", "updated_at"];

    public function tags() {
        return $this->hasMany(Tag::class);
    }

    public function posts() {
        return $this->hasMany(Post::class);
    }
}
