<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentAndLike extends Model
{
    use HasFactory;

    protected $fillable = [
        'value',
        'comment_id',
    ];
    
    protected $hidden = ["created_at", "updated_at"];
    
    public function comments() {
        return $this->hasMany(Comment::class);
    }
}
