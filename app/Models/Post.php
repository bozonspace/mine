<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'user_id',
    ];
    
    protected $hidden = ["created_at", "updated_at"];

    public function tagAndPost() {
        return $this->belongsTo(TagAndPost::class);
    }

    public function comment() {
        return $this->belongsTo(Comment::class);
    }

    public function postAndLikes() {
        return $this->belongsTo(PostAndLike::class);
    }

    public function user() {
        return $this->hasOne(User::class);
    }
}
