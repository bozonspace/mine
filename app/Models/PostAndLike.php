<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostAndLike extends Model
{
    use HasFactory;

    protected $fillable = [
        'value',
        'post_id',
    ];
    
    protected $hidden = ["created_at", "updated_at"];

    public function posts() {
        return $this->hasMany(Post::class);
    }
}
