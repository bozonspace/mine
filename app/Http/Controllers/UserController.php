<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use stdClass;

class UserController extends Controller
{
     /**
     * Get user info
     * 
     * @return JsonResponse
     */
    public function getMe(Request $request)
    {
        $user = Auth::user();
        $data = new stdClass;
        $data->id = $user->id;
        $data->nickname = $user->nickname;
        $data->email = $user->email;
        return response()->json([
            "success"   => true,
            "message"   => 'You found successfully',
            "data"      => $data,
        ]);
    }
}
