<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Validation\Rules\Password;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(Request $request) {
        $fields = $request->validate([
            'nickname' => 'required|string|max:100|alpha_dash|unique:users,nickname',
            'email' => 'required|string|max:255|unique:users,email',
            'password' => ['required', 'confirmed', 'min:6'],
        ]);

        $user = User::create([
            'nickname' => $fields['nickname'],
            'email' => $fields['email'],
            'password' => bcrypt($fields['password']),
            'role_id' => \Config::get('constants.roles.registered'),
        ]);
        // Auth::login($user);
        // $request->session()->regenerate();
        // $user->createToken(env('TOKEN_SALT', 'null'));
        event(new Registered($user)); // for userEmailConfirmation

        $response = [
            'success' => true,
            'path' => RouteServiceProvider::REGISTER_CHECK_EMAIL.'/'.$user->email,
        ];
        return response($response, 201);
    }

    public function login(Request $request) {
        $fields = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        // Check email
        $user = User::where('email', $fields['email'])->first();

        // Check password
        if(!$user || !Hash::check($fields['password'], $user->password)) {
            return response([
                'message' => 'Bad credentials'
            ], 400);
        }

        if(!$user->hasVerifiedEmail()) {
            return response([
                'message' => 'Your email address is not verified.'
            ], 400);
        }

        // if (Auth::attempt($fields)) {
        if (true) {
            Auth::loginUsingId($user->id);
            $request->session()->regenerate();
            $user->createToken(env('TOKEN_SALT', 'null'));
            $response = [
                'success' => true,
                'path' => '/',
            ];
            return response($response, 201);
        }
    }

    /*
    * Logout  
    * @return RedirectResponse
    */
    public function logout(Request $request) {
        $request->user()->tokens()->delete();
        // Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        $response = [
            'success' => true,
            'path' => '/',
        ];
        return response($response, 201);
    }
}
