<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\TagAndPost;
use App\Services\PostService;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Get all posts API
     * 
     * @return JsonResponse
     */
    public function getPosts(Request $request)
    {
        $request->validate([
            'offset'  => 'nullable|string',
            'search'  => 'nullable|string',
        ]);
        $offset = (int)($request->offset);
        $postService = new PostService();
        $data = json_decode($postService->getPosts($offset, $request->search));
        return response()->json([
            "success"   => $data->success,
            "message"   => $data->message,
            "data"      => $data->data,
        ]);
    }

    /**
     * Get post by id API
     * 
     * @return JsonResponse
     */
    public function getPost(Request $request)
    {
        $request->validate([
            'postId'  => 'string',
        ]);
        $postId = (int)($request->postId);
        $postService = new PostService();
        $data = json_decode($postService->getPost($postId));
        return response()->json([
            "success"   => $data->success,
            "message"   => $data->message,
            "data"      => $data->data,
        ]);
    }

    /**
     * Create a post API
     * 
     * @return JsonResponse
     */
    public function postCreate(Request $request)
    {
        $request->validate([
            'text'  => 'required|string|max:150|min:10|unique:posts,title',
            'tags'  => 'required|array|min:1|max:10',
        ]);
        $user = $request->user();
        $userId = $user->id;
        $postService = new PostService();
        $data = json_decode($postService->postCreate($userId, $request->tags, $request->text));
        return response()->json([
            "success"   => $data->success,
            "message"   => $data->message,
            "data"      => $data->data,
            "path"      => '/post/'.$data->data->id
        ]);
    }

    /**
     * Update a post API
     * 
     * @return JsonResponse
     */
    public function postUpdateAPI(Request $request)
    {
        $request->validate([
            'text'   => 'required|string|max:150|min:10',
            'tags'   => 'required|array|min:1|max:10',
            'postId' => 'required|string'
        ]);
        $user = $request->user();
        $userId = $user->id;
        $postService = new PostService();
        $data = json_decode($postService->postUpdate($userId, $request->tags, $request->text, $request->postId));
        return response()->json([
            "success"   => $data->success,
            "message"   => $data->message,
            "data"      => $data->data,
            "path"      => '/post/'.$request->postId
        ]);
    }

    /**
     * Edit a post WEB
     * 
     * @return \Illuminate\Http\RedirectResponse|View
     */
    public function postEdit(Request $request)
    {
        $user = $request->user();
        $postId = intval($request->route('id'));
        $userId = $user->id;
        $post = Post::where([
            ['id','=',$postId],
            ['user_id','=',$userId],
        ])->first();
        if(!$post) {
            return redirect()->intended('/');
        }
        return view('index');
    }

    /**
     * Delete a post API
     * 
     * @return JsonResponse
     */
    public function postDelete(Request $request)
    {
        $user = $request->user();
        $request->validate([
            'postId' => 'required|string'
        ]);
        $postId = intval($request->postId);
        $userId = $user->id;
        $post = Post::where([
            ['id', '=', $postId],
            ['user_id', '=' ,$userId],
        ])->delete();
        if($post) {
            TagAndPost::where('post_id',$postId)->delete();
        }
        return response()->json([
            "success"   => true,
            "message"   => 'Post deleted',
            "data"      => '',
            "path"      => '/'
        ]);
    }
}
